package com.example.david.tiempo.Model;

public class Clouds {

    //Para almacenar las nubes.

    private int all;

    public Clouds(int all) {
        this.all = all;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

}
