package com.example.david.tiempo.Common;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Conversiones {


    // Crea un link util del path
    @NonNull
    public static String apiRequest(String lat, String lng){
        String API_LINK = "http://api.openweathermap.org/data/2.5/weather";
        String API_KEY = "f3c8b87c82e2afa27ca773f9382960dc";
        return API_LINK + String.format("?lat=%s&lon=%s&appid=%s", lat, lng, API_KEY);
    }

    // Para convertir el time stamp de unix a HH:mm
    public static String unixTimeStampToDateTime(double unixTimeStamp){
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        date.setTime((long)unixTimeStamp*1000);
        return dateFormat.format(date);
    }

    // Esta función es para obtener la imagen del tiempo de la pagina.
    public static String getImage(String icon){
        return String.format("http://openweathermap.org/img/w/%s.png",icon);
    }

    // Función para cambiar el formato del dia y la hora a una legible.
    public static String getDateNow(){
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static double kelvinToCelsius(double kelvin){
        return kelvin - 273;
    }


}
