package com.example.david.tiempo.Helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Helper {
    private static String stream = null;

    public Helper() {
    }

    // Funcion para hacer un pedido a la api de OpenWeather Map's

    // Abre una conexion HTTP mediante la URL y si se responde correctamente con HTTP 200 se guarda
    // el mensaje en un stream.
    public String getHTTPData(String urlString){
        try {
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            if(httpURLConnection.getResponseCode() == 200) // OK - 200
            {
                BufferedReader r = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while((line = r.readLine())!=null)
                    sb.append(line);
                stream = sb.toString();
                httpURLConnection.disconnect();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return  stream;
    }
}
